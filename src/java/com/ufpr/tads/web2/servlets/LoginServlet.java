package com.ufpr.tads.web2.servlets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
É uma servlet que verifica o login do usuário. Como ainda não temos acesso ao banco de dados, aceita-se um par login/senha como válido se eles forem iguais (Ex.: 123/123, admin/admin, root/root, etc).

Se o par login/senha é válido:
Apresenta uma tela mostrando:
Uma mensagem indicando que o usuário está logado com sucesso
Um link para a servlet PortalServlet.
Se o par login/senha não é válido:
Apresenta uma tela:
Com a mensagem de usuário/senha não encontrado;
Um link para index.html
 */
import com.ufpr.tads.web2.beans.LoginBean;
import com.ufpr.tads.web2.dao.UsuarioDAO;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.ufpr.tads.web2.beans.Usuario;

/**
 *
 * @author lucas
 */
@WebServlet(urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter("login");
        String senha = request.getParameter("senha");

        UsuarioDAO dao = new UsuarioDAO();
        Usuario usua = dao.buscar(login, senha);
        if(usua != null){
        
            LoginBean lo = new LoginBean();
            lo.setId(usua.getId());
            lo.setNome(usua.getNome());
            
            HttpSession session = request.getSession();
            session.setAttribute("user", lo);

            RequestDispatcher rd = request.
                    getRequestDispatcher("portal.jsp");
            rd.forward(request, response);
        } 
        else {
        

        RequestDispatcher rd = request.
                getRequestDispatcher("erro.jsp");
        request.setAttribute("msg", "erro");
        request.setAttribute("page", "index.html");
        rd.forward(request, response);}

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
