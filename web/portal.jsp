<%-- 
    Document   : portal
    Created on : 12/09/2019, 14:45:15
    Author     : lucas
--%>


<%@page import="com.ufpr.tads.web2.beans.LoginBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>portal jsp</title>
    </head>
    <body>

        <%
            LoginBean user = (LoginBean) pageContext.getSession().getAttribute("user");

            if (user == null) {
                RequestDispatcher rd = request.
                        getRequestDispatcher("erro.jsp");
                request.setAttribute("msg", "erro");
                request.setAttribute("page", "index.html");
                rd.forward(request, response);
            }

        %>

        <h1>Hello User!</h1>
        <jsp:useBean id="lo" class="com.ufpr.tads.web2.beans.LoginBean" scope="session"/>

        Nome: <jsp:getProperty name="user" property="nome"/> <br/>

        <a href="inserir.jsp">inserir</a><br>
        <a href="http://localhost:8080/WebExer2ServletForm/LogoutServlet">logout</a>
        <br>

        <jsp:useBean id="configuracao"
                     class="com.ufpr.tads.web2.beans.ConfigBean"
                     scope="application" />
        E-mail admin: <jsp:getProperty name="configuracao"
                         property="email"/>
    </body>
</html>
