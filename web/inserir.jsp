<%-- 
    Document   : inserir
    Created on : 13/09/2019, 10:57:11
    Author     : lucas
--%>

<%@page import="com.ufpr.tads.web2.beans.LoginBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>inserir jsp</title>
    </head>
    <body>

        <%
            LoginBean user = (LoginBean) pageContext.getSession().getAttribute("user");

            if (user == null) {
                RequestDispatcher rd = request.
                        getRequestDispatcher("erro.jsp");
                request.setAttribute("msg", "erro");
                request.setAttribute("page", "index.html");
                rd.forward(request, response);
            }

        %>

        <h1>inserir</h1>

        <form action="http://localhost:8080/WebExer2ServletForm/CadastrarUsuarioServlet" method="POST">
            nome: <input type="text" name="nome" /> <br/>
            login: <input type="text" name="login"/> <br/>
            senha: <input type="text" name="senha"/> <br/>

            <input type="submit" value="OK" />

        </form>

    </body>
</html>
